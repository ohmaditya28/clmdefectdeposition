package CommonMethod;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class DefectDepositionChoose_Data extends WaitTillElementFound
{
	Logger logger = Logger.getLogger("devpinoyLogger");
public void DefectDepositonSetConditionsDetails(WebDriver d) throws Exception
{
	Actions actions=new Actions(d);
	JavascriptExecutor js = (JavascriptExecutor)d;
	ArrayList<String> Prism_Project_Id=ReadData4(0);
	
	//WebElement PrismProjectIdCheckbox=d.findElement(By.xpath("//div[@class='groups-container']/child::div[3]/child::div/input[@type='checkbox']"));
	WebElement PrismEditValue=d.findElement(By.xpath("//div[@class='groups-container']/child::div[3]/child::div/button[@title='Edit the values for this condition']"));

	js.executeScript("arguments[0].click();", PrismEditValue);
	WaitForEditCondition(d);
	Thread.sleep(500);
	d.findElement(By.xpath("//div[@class='string-filter']/child::input[3]")).clear();
	Thread.sleep(2000);
	d.findElement(By.xpath("//div[@class='string-filter']/child::input[3]")).sendKeys(Prism_Project_Id.get(0));
	Thread.sleep(2000);	
	d.findElement(By.xpath("//button[@class='btn btn-primary btn-bluemix update-filter' and contains(.,'Update')]")).click();
	Thread.sleep(2000);
	WebElement ReleaseEditValue=d.findElement(By.xpath("//div[@class='groups-container']/child::div[4]/child::div/button[@title='Edit the values for this condition']"));
	js.executeScript("arguments[0].click();", ReleaseEditValue);
	WaitForEditCondition(d);
	Thread.sleep(2000);
	WebElement LastFilteredRelease=d.findElement(By.xpath("//button[@id='clear-action']/preceding::div[@class='input-group gadget-input-group-xs filter-select-filter-container']"));
	js.executeScript("arguments[0].click();",LastFilteredRelease);
	WaitForEditCondition(d);
	
	String CurrentReleaseId=d.findElement(By.xpath("//div[@class='filter-options']/span[4]")).getText();
	logger.debug(CurrentReleaseId);
	Thread.sleep(200);
	WebElement DetecteddInReleaseInputbox=d.findElement(By.xpath("//div[@class='input-group gadget-input-group-xs filter-select-filter-container']/following::input[@type='text' and @class='form-control input-sm']"));
	DetecteddInReleaseInputbox.sendKeys(CurrentReleaseId);
	Thread.sleep(7);
	WebElement CurrentReleaseUncheckbox=d.findElement(By.xpath("//ul[@class='list-group filter-select-item-list']/following::input[@type='checkbox' and @value='"+CurrentReleaseId+"']"));
	js.executeScript("arguments[0].click();",CurrentReleaseUncheckbox);
	logger.debug("Detected in Release is now Cleaned");
	Thread.sleep(500);
	DetecteddInReleaseInputbox.clear();
	Thread.sleep(500);
	ArrayList<String> ReleaseId=ReadData4(1);
	DetecteddInReleaseInputbox.sendKeys(ReleaseId.get(0));
	Thread.sleep(500);
	WebElement CurrentReleasecheckbox=d.findElement(By.xpath("//ul[@class='list-group filter-select-item-list' and @tabindex='-1']/following::input[@type='checkbox' and @value='"+ReleaseId.get(0)+"']"));
	js.executeScript("arguments[0].click();",CurrentReleasecheckbox);
	logger.debug("Detected in Release is now Checked");
	Thread.sleep(500);
	d.findElement(By.xpath("//button[@class='btn btn-primary btn-bluemix update-filter' and contains(.,'Update')]")).click();
	Thread.sleep(1000);
	WebElement ApplicationFoundInEditValue=d.findElement(By.xpath("//div[@class='groups-container']/child::div[6]/child::div/button[@title='Edit the values for this condition']"));
	
	js.executeScript("arguments[0].click();",ApplicationFoundInEditValue);
	
	WaitForEditCondition(d);
	d.findElement(By.xpath("//div[@class='string-filter']/child::input[3]")).clear();
	ArrayList<String> ApplicationFoundIn=ReadData4(2);
	d.findElement(By.xpath("//div[@class='string-filter']/child::input[3]")).sendKeys(ApplicationFoundIn.get(0));
	Thread.sleep(500);
	d.findElement(By.xpath("//button[@class='btn btn-primary btn-bluemix update-filter' and contains(.,'Update')]")).click();
	Thread.sleep(500);
	WebElement RunReportClick=d.findElement(By.xpath("//a[@href='#runReportTab']"));
	js.executeScript("arguments[0].click();",RunReportClick);
	Thread.sleep(7000);
	WebElement ReportSavebutton=d.findElement(By.xpath("//div[@class='report-actions']/following::button[contains(.,'Save')]"));
	js.executeScript("arguments[0].click();",ReportSavebutton);
	WaittillReportSavedSuccessfully(d);
    d.navigate().refresh();
    Thread.sleep(3000);
    WaittillExportButtonDisplayed(d);
    	
	
}

public ArrayList<String> ReadData4(int ColNum) throws Exception
{
	String filePath = System.getProperty("user.dir");   
    String fileName="DataExcel.xlsx";
    //String sheetName="test";
    
 File file =    new File(filePath+"\\"+fileName); 

//FileInputStream fis=new FileInputStream("C:\\Selenium Automation Program\\Data\\DataExcel.xlsx");

XSSFWorkbook workbook=new XSSFWorkbook(file);

XSSFSheet sheet=workbook.getSheet("DefectDeposition");


Iterator<Row> rowIterator=sheet.iterator();

rowIterator.next();

ArrayList<String> list=new ArrayList<String>();

while(rowIterator.hasNext())
{

	list.add(rowIterator.next().getCell(ColNum).getStringCellValue());
}
logger.debug("Values from Excel Loaded in Collections");
//System.out.println(list);
return list;
	
}
}
