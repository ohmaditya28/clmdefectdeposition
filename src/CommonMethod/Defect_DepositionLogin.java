package CommonMethod;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Defect_DepositionLogin extends WaitTillElementFound
{
	Logger log = Logger.getLogger("devpinoyLogger");
	
	public ArrayList<String> CLMITServicesLogin(WebDriver d ) throws Exception
	{
		ArrayList<String> UserName=ReadData3(0);
		ArrayList<String> Password=ReadData3(1);
		
		for(int i=0;i<UserName.size();i++)
		{
			
		try {
				
				d.findElement(By.name("j_username")).sendKeys(UserName.get(0));
				Thread.sleep(500);
				d.findElement(By.name("j_password")).sendKeys(Password.get(0));
				Thread.sleep(500);
				d.findElement(By.xpath("//button[contains(.,'Log In')]")).click();
				WaitforJazzHomeReportScreen(d);
				
			}
			catch(Exception e)
			{
				System.out.println(e.getStackTrace());
				logger.debug(e);
			}
	    }
		return Password;
	}
	
	public ArrayList<String> ReadData3(int ColNum) throws Exception
	{
		String filePath = System.getProperty("user.dir");   
	    String fileName="DataExcel.xlsx";
	    //String sheetName="test";
	    
	 File file =    new File(filePath+"\\"+fileName); 
	//FileInputStream fis=new FileInputStream("C:\\Selenium Automation Program\\Data\\DataExcel.xlsx");

	//XSSFWorkbook workbook=new XSSFWorkbook(fis);
	XSSFWorkbook workbook=new XSSFWorkbook(file);

	XSSFSheet sheet=workbook.getSheet("Defect_ITServices");


	Iterator<Row> rowIterator=sheet.iterator();

	rowIterator.next();

	ArrayList<String> list=new ArrayList<String>();

	while(rowIterator.hasNext())
	{

		list.add(rowIterator.next().getCell(ColNum).getStringCellValue());
	}
	log.debug("Values from Excel Loaded in Collections");
	//System.out.println(list);
	return list;
	
	
}
}
