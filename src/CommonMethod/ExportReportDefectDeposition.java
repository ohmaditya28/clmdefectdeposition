package CommonMethod;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

public class ExportReportDefectDeposition extends WaitTillElementFound
{
public void ExportReportExcel(WebDriver d) throws Exception
{
	
	Actions actions = new Actions(d);
	JavascriptExecutor js = (JavascriptExecutor)d;

	System.out.print("In Frame");
	WebElement ExportDropdownbtn=d.findElement(By.xpath("//button[@id='export-drop-down']"));
	js.executeScript("arguments[0].click();",ExportDropdownbtn);
	Thread.sleep(500);
	WebElement ExportTheExcelclick=d.findElement(By.xpath("//a[contains(.,'Export the report to Microsoft Excel')]"));
	js.executeScript("arguments[0].click();",ExportTheExcelclick);

	
}

public void ExportExcelReportSwitchWindow(WebDriver d) throws Exception
{
	String ParentReportGeneratedWindow=d.getWindowHandle();	
	for (String SubWindowExportWindow:d.getWindowHandles())
	{
		d.switchTo().window(SubWindowExportWindow);
	}
	WaitTillExportExcelNewWindow(d);
	d.findElement(By.xpath("//a[contains(.,'Download a spreadsheet with live data.')]")).click();
	
	Thread.sleep(5000);
	
	Screen s1=new Screen();

	Pattern p1=new Pattern("Images\\DefectDespositionReportClickSaveFile.PNG");
	Pattern p2=new Pattern("Images\\DefectDespositionReportClickokbutton.PNG");
	Pattern p3=new Pattern("Images\\DefectDespositionReportClickSaveFile.PNG");
	
	s1.wait(p3,100);
	s1.click(p1);
	Thread.sleep(1000);
	s1.click(p2);
	logger.debug("Report Saved,Checked Download Folder");
	
}
}
