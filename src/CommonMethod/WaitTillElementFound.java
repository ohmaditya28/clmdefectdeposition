package CommonMethod;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WaitTillElementFound
{
	Logger logger = Logger.getLogger("devpinoyLogger");
public void WaitforLoginScreen(WebDriver d)
{
	WebDriverWait waitforElement=new WebDriverWait(d, 10);
	
	waitforElement.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(.,'Jazz Authorization Server')]")));
	System.out.println("CLM Login Screen is Visible");
	logger.debug("CLM Login Screen is Visible");
}

public void WaitforJazzHomeReportScreen(WebDriver d)
{
	WebDriverWait waitforElement=new WebDriverWait(d, 10);
	waitforElement.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@href='#mine']")));
	System.out.println("Jazz Home Page is visible");
	logger.debug("Jazz Home Page is visible");
		
}
public void WaitforMyReportsVBCarePage(WebDriver d)
{
	WebDriverWait waitforElement=new WebDriverWait(d, 20);
	waitforElement.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@href='reportdefinition/7869/../../reports#view=7869']")));
	System.out.println("All Reports VB Care Page is visible");
	logger.debug("All Reports VB Care Page is visible");
		
}
public void WaitForMyReportsVBCareEditPage(WebDriver d)
{
	WebDriverWait waitforElement=new WebDriverWait(d, 10);
	waitforElement.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='view-report-content-header']")));
	System.out.println("VB Care Edit Page is reached");
	logger.debug("VB Care Edit Page is reached");
	
}



public void WaitforNameSharetoLoad(WebDriver d)
{
	WebDriverWait waitforElement=new WebDriverWait(d,30);
	waitforElement.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@id='report-creator' and contains(text(),'oa4708')]")));
	System.out.println("Name&Share Data page is successfully loaded");
	logger.debug("Name&Share Data page is successfully loaded");
	
}

public void WaitforChooseData(WebDriver d)
{
	WebDriverWait waitforElement=new WebDriverWait(d,10);
	waitforElement.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@href='#reportType-tab']")));
	System.out.println("Choose Data page is successfully loaded");
	logger.debug("Choose Data page is successfully loaded");
}

public void waitforChooseDataSetConditions(WebDriver d)
{
	WebDriverWait waitforElement=new WebDriverWait(d,10);
	waitforElement.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h4[@class='choose-attribute-help text-muted' and contains(.,'Saved conditions')]")));
	System.out.println("Choose Data page is successfully loaded");
	logger.debug("Choose Data page is successfully loaded");
}

public void waitforChoosePrismid(WebDriver d)
{
	WebDriverWait waitforElement=new WebDriverWait(d,10);
	waitforElement.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(.,'(Work Item) PRISM Project ID, Project id (Custom)')]")));
	System.out.println("Prism Project id is successfully loaded");
	logger.debug("Prism Project id is successfully loaded");
}

public void WaitForEditCondition(WebDriver d)
{
	WebDriverWait waitforEditCondition=new WebDriverWait(d,20);
	
	waitforEditCondition.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h4[@id='add-filters-title' and contains(text(),'Edit condition')]")));
	System.out.println("Edit Condition Popup for is successfully loaded");
	logger.debug("Edit Condition Popup is successfully loaded");
			
}

public void WaitForTableReportGeneration(WebDriver d)
{
WebDriverWait waitforEditCondition=new WebDriverWait(d,120);
    d.switchTo().frame("run-report-frame");
	waitforEditCondition.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(),'Items Per Page')]")));
	d.switchTo().defaultContent();
	System.out.println("Table Report is Generated");
	logger.debug("Table Report is Generated");
	
}

public void WaittillReportSavedSuccessfully(WebDriver d)
{
WebDriverWait waitforEditCondition=new WebDriverWait(d,60);

	waitforEditCondition.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(.,'Successfully saved the updated report.')]")));
	System.out.println("Successfully saved the updated report");
	logger.debug("Successfully saved the updated report");
}

public void WaittillExportButtonDisplayed(WebDriver d) throws Exception
{
	WebDriverWait waitforEditCondition=new WebDriverWait(d,120);
	
	d.switchTo().frame(1);
	Thread.sleep(1500);
	//waitforEditCondition.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[@id='export-drop-down']")));
	waitforEditCondition.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(),'Items Per Page')]")));
	System.out.println("Export Button is enabled for downloading the report");
	logger.debug("Export Button is enabled for downloading the report");
}

public void WaitTillExportExcelNewWindow(WebDriver d)
{
	WebDriverWait waitforEditCondition=new WebDriverWait(d,50);
	waitforEditCondition.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[contains(.,'Export the report to Microsoft Excel')]")));
	System.out.println("Export the report to Microsoft Excel");
	logger.debug("Export the report to Microsoft Excel");
}
}
