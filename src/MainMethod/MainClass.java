package MainMethod;

import org.apache.log4j.Logger;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import CommonMethod.DefectDepositionChoose_Data;
import CommonMethod.DefectDeposition_MyReports;
import CommonMethod.Defect_DepositionLogin;
import CommonMethod.ExportReportDefectDeposition;
import ObjectRepository.URLRepository;

public class MainClass extends URLRepository {

	public static void main(String[] args) throws Exception {
		Logger logger = Logger.getLogger("devpinoyLogger");
		
		System.setProperty("webdriver.gecko.driver","C:\\Automation_Pract\\Selenium\\MozilaDriver\\geckodriver.exe");
		
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities = DesiredCapabilities.firefox();
	    capabilities.setBrowserName("firefox");
		capabilities.setVersion("your firefox version");
	    capabilities.setPlatform(Platform.WINDOWS);
	    capabilities.setCapability("marionette", false);
		WebDriver driver = new FirefoxDriver(capabilities);
		driver.manage().window().maximize();
		driver.get(DefectDepositionURL);
		Thread.sleep(2000);
		logger.debug("URL Launched");
		Thread.sleep(700);
		
		Defect_DepositionLogin DDL=new Defect_DepositionLogin();
		DDL.ReadData3(0);
		DDL.CLMITServicesLogin(driver);
		
		DefectDeposition_MyReports DDMR=new DefectDeposition_MyReports();
		DDMR.MyReportsonJazzHomePage(driver);
		DefectDepositionChoose_Data DDCD=new DefectDepositionChoose_Data();
		DDCD.DefectDepositonSetConditionsDetails(driver);
		ExportReportDefectDeposition ERDD=new ExportReportDefectDeposition();
		ERDD.ExportReportExcel(driver);
		Thread.sleep(2000);
		ERDD.ExportExcelReportSwitchWindow(driver);
		
		
		
		
	

	

	}

}
